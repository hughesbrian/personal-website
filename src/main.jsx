import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
import SideBar from './sidebar/SideBar';
import Header from './header/Header';
import Footer from './footer/Footer';
import Timeline from './timeline/Timeline';
import './index.css'

createRoot(document.getElementById('root')).render(
  <StrictMode>
        <SideBar />

          <Header title="WELCOME" blurb={[ "I’m Brian Hughes, a Philadelphia based DevOps Engineer working remote for DonorDrive. I’m passionate about DevOps, IaC, CI/CD pipelines, and scripting; anything I can do to automate processes for myself or my teammates is what excites me!",
                                            "In my free time I like to spend time with my wife Alex and my cats Celeste, Dandelion, Eva, and French Fry. I also enjoy gardening, cooking, and keeping up with video games."]}
          />
          <Timeline timelineitems={[
              {
                time: "August 2022",
                title: "Junior Site Reliability Engineer ",
                company: "DonorDrive",
                desc:
                [
                  "Worked directly with lead DevOps Engineer and CTO to manage infrastructure across all AWS accounts and Cloudflare, using Terraform for IaC",
                  "Participated in the on-call rotation and independently resolved production issues",
                  "Enhanced the monitoring framework by integrating alerts from both CloudWatch and the SIEM tool LevelBlue to PagerDuty",
                  "Migrated on-prem Bitbucket to Bitbucket Cloud including all CI/CD pipelines from Bamboo to Bitbucket Pipelines",
                  "Led the migration and standardization of Cloudflare WAF rules leveraging Terraform and Bash",
                  "Implemented Cloudflare WARP VPN enabling developer access to AWS resources",
                  "Created and managed ECR repositories for development images and internal tools",
                  "Developed AWS Lambda functions in Python to automate recurring tasks, such as cleaning up unused AMIs and performing database health checks",
                  "Configured EC2 runners to automate OWASP ZAP scans in compliance with PCI requirements and implemented daily masked database refreshes",
                  "Ran OS updates on the entire infrastructure including overnight production database patches",
                  "Documented the patching process in detail and contributed to agile development practices like code reviews"
                ]
              },
              {
                time: "July 2020",
                title: "DevOps Engineer I",
                company: "PowerAdvocate",
                desc:
                [
                  "Started as a Co-op from July 2020 to April 2021, returned in June 2021 as a DevOps Engineer",
                  "Led the development of a DevOps team documentation including releases, OS updates, hiring, infrastructure architecture, and onboarding process",
                  "Oversaw the DevOps Co-op program: interviewed potential candidates, planned onboarding, conducted training, and mentored 4 Co-ops",
                  "Independently ran deployments of all applications",
                  "Implemented updates to the ECS deployment automation tool to add new microservices",
                  "Tightened IAM permissions for microservices to reduce operational risk",
                  "Served as the point of contact between Data Science and DevOps teams",
                  "Collaborated with the DevOps team to migrate Artifactory from on-premises to AWS"]
              },
              {
                time: "July 2019 - December 2019",
                title: "DevOps Co-op",
                company: "Wanderu",
                desc:
                [
                  "Updated Icinga and AWS SNS monitoring systems to improve site reliability and application health",
                  "Implemented pilot and production auto-deployments through a Python deployment application",
                  "Created nginx rewrites and updated configurations"
                  ]
              }
            ]}/>
          <Footer/>
  </StrictMode>,
)
